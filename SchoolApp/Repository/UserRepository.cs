﻿using System;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RepositoryLib.Data;
using SchoolApp.Data;
using SchoolApp.Entities;

namespace SchoolApp.Repository
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        User GetUserByEmail(string email);
    }

    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        protected DbSet<User> RepositoryContext { get; set; }

        public UserRepository(SchoolDbContext repositoryContext) : base(repositoryContext)
        {
            RepositoryContext = repositoryContext.Set<User>();
        }

        public User GetUserByEmail(string email)
        {
            return RepositoryContext.SingleOrDefault(x => x.Email.ToLower().Equals(email.Trim().ToLower()));
        }
    }
}
