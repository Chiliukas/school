﻿using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using SchoolApp.Models;
using SchoolApp.System;
using System.Net.Mail;
using Entities;
using RepositoryLib.Data;

namespace SchoolApp.Controllers
{
    [Route("{controller}")]
    public class AuthController : Controller
    {
        private IUserRepository _userRepository;

        public AuthController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [Route("login")]
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [Route("login")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult Login(LoginViewModel login)
        {
            var user = _userRepository.GetUserByEmail(login.Email);

            if (user != null && AuthHash.CompareHash(login.Password, user.Password))
            {
                SingInUser(user);
            }
            else
            {
                ViewBag.DublicateMessage = "Bad user name or password";
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        [Route("logout")]
        [HttpPost]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [Route("registration")]
        [HttpGet]
        public IActionResult Registration()
        {
            return View();
        }

        [Route("registration")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Registration(RegistrationViewModel registrationData)
        {
            ViewBag.DublicateMessage = string.Empty;

            if (registrationData == null)
                return View("Registration");

            try
            {
                var addr = new MailAddress(registrationData.Email);
            }
            catch
            {
                ViewBag.DublicateMessage = "Invalid Email Format";
                return View("Registration");
            }

            if (string.IsNullOrEmpty(registrationData.Password) || registrationData.Password != registrationData.ConfirmPassword)
            {
                ViewBag.DublicateMessage = "Confirm password doesn't match, Type again!";
                return View("Registration");
            }

            if (_userRepository.GetUserByEmail(registrationData.Email) != null)
            {
                ViewBag.DublicateMessage = "Email already exist.";
                return View("Registration");
            }

            var user = new User()
            {
                FirstName = registrationData.FirstName,
                LastName = registrationData.LastName,
                Email = registrationData.Email,
                Password = AuthHash.MakeHash(registrationData.Password)
            };

            _userRepository.Add(user);
            return RedirectToAction("Index", "Home");
        }

        private IActionResult SingInUser(User user)
        {
            var  claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Email),
                new Claim(ClaimTypes.Name, user.LastName),
                new Claim(ClaimTypes.Email, user.Email)
            };

            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);

            HttpContext.SignInAsync(principal);

            return RedirectToAction("Index", "Home");
        }
    }
}