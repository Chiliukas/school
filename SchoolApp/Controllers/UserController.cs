﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RepositoryLib.Data;

namespace SchoolApp.Controllers
{
    [Route("{controller}")]
    [Authorize]
    public class UserController : Controller
    {
        private IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [Route("schoolchildrenlist")]
        public IActionResult SchoolchildrenList()
        {
            return View(_userRepository.FindAll());
        }
    }
}