﻿using Microsoft.AspNetCore.Mvc;
using RepositoryLib.Data;

namespace SchoolApp.Controllers
{
    [Route("{controller}")]
    public class HomeController : Controller
    {
        public IUserRepository _userRepository;

        public HomeController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [Route("Index")]
        [Route("~/")]
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
    }
}