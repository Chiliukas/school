﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolApp.Models
{
    public class RegistrationViewModel
    {
        [DisplayName("First name")]
        [Required(ErrorMessage = "This field is requared")]
        public string FirstName { get; set; }

        [DisplayName("Last name")]
        [Required(ErrorMessage = "This field is requared")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "This field is requared")]
        [EmailAddress(ErrorMessage = "Enter correct email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is requared")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Confirm password")]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again!")]
        public string ConfirmPassword { get; set; }
    }
}
